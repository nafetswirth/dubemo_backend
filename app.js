/*
* @Author: herby
* @Date:   2016-05-21 11:40:34
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2016-05-22 04:25:01
*/

'use strict';

var express = require('express');
var multer  = require('multer');

var images  = require('./routes/emotion');
var videos  = require('./routes/video');
var speech  = require('./routes/speech');

var PORT = process.env.PORT || 8080;

var app = express();
var upload = multer({ dest: null, inMemory: true });


app.get('/videos', videos);
app.get('/speeches', speech.find);

app.post('/emotions', upload.single('file'), images.emotions);
app.post('/speeches', upload.single('file'), speech.convert);

app.listen(PORT, function() {
    console.log('listening on ' + PORT);
});