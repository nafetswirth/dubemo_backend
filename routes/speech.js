/*
* @Author: Dat Dev
* @Date:   2016-05-21 21:04:45
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2016-05-22 09:55:48
*/

'use strict';

var watson = require('watson-developer-cloud');
var streamifier = require('streamifier');
var Cloudconvert = require('cloudconvert');
var fs = require('fs');

var giphyService = require('../services/giphy');

var WATSON_URL = 'https://stream.watsonplatform.net/speech-to-text/api';
var WATSON_PASSWORD   = 'J3A5CF8OFHTe';
var WATSON_USER_NAME  = '6a2d8ace-0d7e-4f6a-a055-921336ad80b3';
var FALLBACK_GIF_TERM = 'cat';

var CLOUD_CONVERT_API_KEY = 'ttxaQJNhuBtPHAe6oFq_8z4MXdD1Whcn_6bzsmkhVxUg9MvbeFf-hZIiayMCGhKQ5ycNjZuwPy3rbcWPFdQdpg';

var cloudConvert = new Cloudconvert(CLOUD_CONVERT_API_KEY);

module.exports = {
    convert: convert,
    find: find
};

function convert(req, res) {
    var file = req.file;

    if(!file) {
        return res.status(400, {error: 'Missing file'});
    }

    console.log(file); 

    var speechToText = watson.speech_to_text({
      username: WATSON_USER_NAME,
      password: WATSON_PASSWORD,
      version: 'v1',
      url: WATSON_URL
    });

    var recognizeStream = speechToText.createRecognizeStream(params);

    recognizeStream.setEncoding('utf8');
    recognizeStream.on('data', function(data) {
        console.log(data);
        var term = decodeURIComponent(data) || FALLBACK_GIF_TERM;
        var result = term.match(/me (\w+) w*/);
        var mainTerm = (result || []).pop() || FALLBACK_GIF_TERM;
        return res.status(200).json({term: mainTerm});
    });

    recognizeStream.on('error', function(error) {
        console.log(error);
        return res.status(500).json({error: error});
    });

    var params = {
        content_type: 'audio/wav',
        continuous: true,
        interim_results: false
    };
     
    streamifier.createReadStream(file.buffer).pipe(recognizeStream);
}

function find(req, res) {
    var term = req.query.term || FALLBACK_GIF_TERM;
    giphyService.find(term + ' movie')
        .then(function(giphs) {
            res.status(200).json(giphs);
        })
        .catch(function(err) {
            return res.status(500).json({error: err});
        });
}