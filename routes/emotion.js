/*
* @Author: herby
* @Date:   2016-05-21 12:13:07
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2016-05-22 07:11:33
*/

'use strict';

var fs = require('fs');
var request = require('request');
var streamifier = require('streamifier');

var MS_API_TOKEN = process.env.MS_API_TOKEN || '8e291d6a73074492bec2d0cf13555a05';
var MS_API_URL   = 'https://api.projectoxford.ai/emotion/v1.0/recognize';
var STUB_RESPONSE = {emotions: [
        'neutral',
        'contempt'
    ]
};

module.exports = {
    emotions: emotions
}

function emotions(req, res) {

    var stub = req.query.stub || false;

    if(stub) {
        return stubEmotions(req, res);
    }

    var file = req.file;

    console.log(file);

    if(!file) {
        return res.status(400).json({error: 'Missing file'});
    }

    var emoRequest = request.post({
        url: MS_API_URL,
        headers: {
            'Content-Type' : 'application/octet-stream',
            'Ocp-Apim-Subscription-Key' : MS_API_TOKEN
        }
    }, function(err, emotionRes, body) {
        if(err) {
            console.log(err);
            return res.status(500).json({error: err});
        }

        console.log(body);

        var emotions = (JSON.parse(body).shift() || {}).scores || {neutral: 0, contempt: 0};
        
        var topEmotions = Object.keys(emotions).map(function(emotionKey) {
            var score = emotions[emotionKey];
            var transformedScore = {};
            transformedScore[emotionKey] = score;
            return transformedScore;
        })
        .sort(function(a, b) {
            return a[Object.keys(a)[0]] > b[Object.keys(b)[0]] ? -1 : 1;
        })
        .slice(0, 2)
        .map(function(score) {
            return Object.keys(score)[0];
        });
        console.log('Sending emotion response');
        console.log(emotions);
        res.status(200).json({emotions: topEmotions});
    });

    streamifier.createReadStream(file.buffer).pipe(emoRequest);
};

function stubEmotions(req, res) {
    res.status(200).json(STUB_RESPONSE);
}