/*
* @Author: Dat Dev
* @Date:   2016-05-21 15:52:23
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2016-05-21 22:41:07
*/

'use strict';

var request = require('request');
var giphyService = require('../services/giphy');

module.exports = function(req, res) {
    var emotion = req.query.emotion || 'hapiness';
    var query = 'movie ' + emotion;
    
    giphyService.find(query)
        .then(function(giphs) {
            res.status(200).json(giphs);
        })
        .catch(function(err) {
            return res.status(500).json({error: err});
        });
};