/*
* @Author: Dat Dev
* @Date:   2016-05-21 22:32:45
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2016-05-22 03:30:52
*/

'use strict';

var request = require('request');
var Promise = require('bluebird');

var GIPHY_API_URL = 'http://api.giphy.com/v1/gifs/search';
var GIPHY_API_KEY = 'dc6zaTOxFJmzC';

module.exports = {
    find: find
};

function find(query) {
    return new Promise(function(resolve, reject) {
        return request.get({
            url: GIPHY_API_URL,
            qs: {
                'api_key': GIPHY_API_KEY,
                'q' : query 
            }
        }, function(err, res, body) {
            if(err) {
                return reject(err);
            }
            var giphs = (JSON.parse(body).data || []).map(function(giph) {
                var giphName = giph.slug.split('-');
                var joinedName = giphName.slice(0, giphName.length - 1).join(' ');
                var videoUrl = giph.images.original.mp4;
                var thumbnailImageUrl = giph.images.original_still.url;
                
                return {
                    name: joinedName,
                    videoUrl: videoUrl,
                    thumbnailImageUrl: thumbnailImageUrl
                };
            });
            return resolve(giphs);
        });
    });
}